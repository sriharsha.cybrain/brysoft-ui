export const environment = {
  production: false,
  environment: 'local',
  default_page_size: 50,
  jwt_refresh_ttl_hours: 0,
  jwt_refresh_ttl_minutes: 60 * 30,
  content_type: 'application/json',
  multipart_formdata: 'multipart/form-data',
  client_host: 'http://localhost:7897',
  Services: { api_base_uri: "https://localhost:44317" }

};

