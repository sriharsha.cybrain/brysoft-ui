import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {

  constructor(private _loading: NgxSpinnerService, private toastr: ToastrService) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this._loading.show();
    return next.handle(req).pipe(
      retry(1), finalize(() => {
        this._loading.hide();
      }),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          // client-side error
          //errorMessage = `Error: ${error.error.message}`;
        } else {
          // server-side error
          errorMessage = `Error: ${error.error.message}`;
          //errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;

        }
        this._loading.hide();
        this.toastr.error(errorMessage);
        return throwError(error);
      })
    );
  }
}

