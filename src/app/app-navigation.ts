import { Injectable } from '@angular/core';
import { Roles, Types, Tags, Modules, SubModules } from 'src/app/shared/enums';

export interface IChildren {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  tag: string;
  icon?: string;
  url?: string;
}

export interface IModules extends IChildren {
  children?: Array<IChildren>;
}

export interface INavigation {
  id: string;
  title: string;
  tag: string;
  modules?: Array<IModules>;
}

const Navigation = [
  {
    id: '1',
    title: 'Main Navigation',
    tag: Tags.MAIN_NAVIGATION,
    modules: [
      {
        id: '1.1',
        title: 'Dashboard',
        type: Types.COLLAPSE,
        tag: Modules.DASHBOARD,
        icon: 'pe-7s-home',
        children: [
          {
            id: '1.1.1',
            title: 'Analysis',
            type: Types.ITEM,
            tag: SubModules.HOME,
            url: '/',
          },
        ],
      },
    ],
  },
  {
    id: '2',
    title: 'Manage Pages',
    tag: Tags.MANAGE_PAGES1,
    modules: [
      {
        id: '2.1',
        title: 'Manage User',
        type: Types.COLLAPSE,
        tag: Modules.MANAGE_PAGES,
        icon: 'pe-7s-display2',
        children: [
          {
            id: '2.1.1',
            title: 'Capture Users',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_USERS,
            url: '/manage user/capture-users',
          },
         
        ],
      },

      {
        id: '2.2',
        title: 'Manage Company',
        type: Types.COLLAPSE,
        tag: Modules.MANAGE_PAGES,
        icon: 'pe-7s-display2',
        children: [
          {
            id: '2.2.1',
            title: 'Capture Company',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_CHART,
            url: '/manage company/capture-company',
          },
         
        ],
      },
      
    ],
  },
  {
    id: '3',
    title: 'Settings',
    tag: Tags.SETTINGS,
    modules: [
      {
        id: '3.1',
        title: 'General',
        type: Types.COLLAPSE,
        tag: Modules.GENERAL,
        icon: 'pe-7s-config',
        children: [
          {
            id: '3.1.1',
            title: 'Capture Group',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_GROUP,
            url: '/general/capture-group',
          },
          {
            id: '3.1.2',
            title: 'Capture Tag',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_TAG,
            url: '/general/capture-tag',
          },
          {
            id: '3.1.3',
            title: 'Capture Brand',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_BRAND,
            url: '/general/capture-brand',
          },
          {
            id: '3.1.4',
            title: 'Capture Type',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_TYPE,
            url: '/general/capture-type',
          },
          {
            id: '3.1.5',
            title: 'Capture Contact',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_CONTACT,
            url: '/general/capture-contact',
          },
        ],
      },
      {
        id: '3.2',
        title: 'User',
        type: Types.COLLAPSE,
        tag: Modules.USERS,
        icon: 'pe-7s-users',
        children: [
          {
            id: '3.2.1',
            title: 'Capture Users',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_USERS,
            url: '/user/capture-users',
          },
          {
            id: '3.2.2',
            title: 'Capture Customers',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_CUSTOMERS,
            url: '/user/capture-customers',
          },
          {
            id: '3.2.3',
            title: 'Capture Members',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_MEMBERS,
            url: '/user/capture-members',
          },
          {
            id: '3.2.4',
            title: 'Capture Suppliers',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_SUPPLIER,
            url: '/user/capture-suppliers',
          },
          // {
          //   id: '3.2.5',
          //   title: 'My Profile',
          //   type: Types.ITEM,
          //   tag: SubModules.MY_PROFILE,
          //   url: '/user/my-profile',
          // },
        ],
      },
    ],
  },
];


const NavigationMember = [
  {
    id: '1',
    title: 'Main Navigation',
    tag: Tags.MAIN_NAVIGATION,
    modules: [
      {
        id: '1.1',
        title: 'Dashboard',
        type: Types.COLLAPSE,
        tag: Modules.DASHBOARD,
        icon: 'pe-7s-home',
        children: [
          {
            id: '1.1.1',
            title: 'Analytics',
            type: Types.ITEM,
            tag: SubModules.HOME,
            url: '/',
          },
        ],
      },
    ],
  },
  {
    id: '2',
    title: 'Manage Chart',
    tag: Tags.MANAGE_PAGES1,
    modules: [
      {
        id: '2.1',
        title: 'Chart',
        type: Types.COLLAPSE,
        tag: Modules.MANAGE_PAGES,
        icon: 'pe-7s-display2',
        children: [
          {
            id: '2.1.1',
            title: `My Chart`,
            type: Types.ITEM,
            tag: SubModules.TODAYS_CHART,
            url: '/chart/my-chart',
          }
        ],
      }
    ],
  },
  {
    id: '3',
    title: 'Settings',
    tag: Tags.SETTINGS,
    modules: [
      {
        id: '3.1',
        title: 'General',
        type: Types.COLLAPSE,
        tag: Modules.GENERAL,
        icon: 'pe-7s-config',
        children: [
          {
            id: '3.1.1',
            title: 'Capture Contact',
            type: Types.ITEM,
            tag: SubModules.CAPTURE_CONTACT,
            url: '/general/capture-contact',
          },
        ],
      }
    ],
  },
];
@Injectable()
export class AppNavigation {
  public get(role: string = Roles.ROOT): any {
    return (role === Roles.MEMBER) ? NavigationMember : Navigation;
  }
}
