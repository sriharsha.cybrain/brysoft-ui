import {Component, Input, OnInit} from '@angular/core';
import { faStar, faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ui-breadcrumb',
  templateUrl: './ui-breadcrumb.component.html',
  styleUrls: ['./ui-breadcrumb.component.css']
})
export class UiBreadcrumbComponent implements OnInit {

  faStar = faStar;
  faPlus = faPlus;

  @Input() heading: any;
  @Input() subheading: any;
  @Input() icon: any;

  constructor() { }

  ngOnInit() {
  }

}
