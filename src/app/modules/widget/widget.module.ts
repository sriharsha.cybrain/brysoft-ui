import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { UiBreadcrumbComponent } from './ui'

@NgModule({
  declarations: [UiBreadcrumbComponent],
  imports: [CommonModule, FontAwesomeModule],
  exports: [FontAwesomeModule,PerfectScrollbarModule, UiBreadcrumbComponent]
})
export class WidgetModule { }
