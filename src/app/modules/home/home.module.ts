import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { WidgetModule } from '../widget/widget.module';
import { MainComponent } from './pages';

@NgModule({
  declarations: [
    MainComponent,
  ],
  imports: [
    CommonModule,
    WidgetModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
