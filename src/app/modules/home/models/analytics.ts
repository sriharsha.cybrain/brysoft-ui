export class IAnalytics {
  customers: number = 0;
  members: number = 0;
  rebateGains: number = 0;
  suppliers: number = 0;
  users: number = 0;
  charts: number = 0;
}
