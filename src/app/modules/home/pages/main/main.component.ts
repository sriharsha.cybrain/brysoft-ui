import { Component, OnInit } from '@angular/core';
import { faTh, faCheck, faTrash, faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { AppFormBase } from 'src/app/app-form-base.component';
import { IAnalytics } from '../../models/analytics';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent extends AppFormBase implements OnInit {
  dataset: IAnalytics = {
    customers: 0,
    members: 0,
    rebateGains: 0,
    suppliers: 0,
    users: 0,
    charts: 0
  };

  ngOnInit(): void {
    this.init();
  }
  
  async init() { }
}
