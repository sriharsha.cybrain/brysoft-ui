import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseContainer, PageContainer } from './layouts';

const routes: Routes = [
  
  {
    path: '',
    component: BaseContainer,
    canActivate: [],
    children: [
      {
        path: '',
        component: PageContainer,
        data: { extraParameter: 'dashboardsMenu' },
        loadChildren: () => import('./modules/home/home.module').then((module) => module.HomeModule),
      },
    ],
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
