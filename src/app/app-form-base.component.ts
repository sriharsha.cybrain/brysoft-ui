import { Component } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { FormArray, FormGroup } from '@angular/forms';
import { AppCommonBase } from './app-common-base.component';

@Component({
  template: ''
})
export class AppFormBase extends AppCommonBase {

  public params: Subject<any> = new Subject<any>();
  public htmlForm!: FormGroup;

  constructor() {
    super();
  }

  async checkValidation() {
    console.log(`Form Validation Error\n`);
    for (const i in this.htmlForm.controls) {
      if (this.htmlForm.controls[i].status === 'INVALID' && window.location.href.includes('localhost')) {
        console.log(`Validation ${i} `, this.htmlForm.controls[i], '\n');
      }
      if (this.htmlForm.controls.hasOwnProperty(i)) {
        this.htmlForm.controls[i].markAsTouched({ onlySelf: true });
        this.htmlForm.controls[i].markAsDirty();
        this.htmlForm.controls[i].updateValueAndValidity();
        if (this.htmlForm.controls[i].hasOwnProperty('controls')) {
          (<FormArray>this.htmlForm.controls[i]).controls.forEach(c => {
            c.markAsDirty();
            c.updateValueAndValidity();
            c.markAsTouched({ onlySelf: true });
          })
        }
      }
    }
  }

}
