import { Component } from '@angular/core';
import { ThemeOptions } from '../theme-options';

@Component({
  selector: 'app-base-layout',
  template: `
       <router-outlet></router-outlet>
  `,
})
export class BaseContainer {
  constructor(public globals: ThemeOptions) {
  }

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }
}
