import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ThemeOptions } from '../../../theme-options';

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
})
export class UserBoxComponent implements OnInit {

  public i_nameid: number | undefined;

  constructor(public globals: ThemeOptions, private router: Router) {
    this.i_nameid = localStorage.getItem('i_nameid') as any;
  }

  ngOnInit() {

  }

  async logout() {
    localStorage.clear();
    this.router.navigateByUrl(environment.client_host + '/auth/sign-in');
  }
}
