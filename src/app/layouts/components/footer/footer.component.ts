import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public copyrightyear : number = new Date().getFullYear();
  public webUrl: string = 'https://www.google.co.in';

  constructor() { }

  ngOnInit() {
  }

}
