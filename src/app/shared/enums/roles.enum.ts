export enum Roles {
  ROOT = 'ADMINISTRATOR',
  ADMIN = 'ADMIN',
  MEMBER = 'MEMBER',
}
