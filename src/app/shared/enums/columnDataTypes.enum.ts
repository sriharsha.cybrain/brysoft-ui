export enum ColumnDataType {
  ALPHA_NUMERIC = 'Alpha Numeric',
  NUMERIC = 'Numeric',
  LONG_TEXT = 'Long Text'
}
