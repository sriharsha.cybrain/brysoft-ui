export enum Modules {
  DASHBOARD = 'pp_dashboard',
  MANAGE_PAGES = 'pp_chart',
  CHART_REPORTS = 'pp_chart_reports',
  GENERAL = 'pp_general',
  USERS = 'pp_users',
}
