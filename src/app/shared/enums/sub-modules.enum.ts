export enum SubModules {
  HOME = 'pp_home',

  CAPTURE_CHART = 'pp_capture_chart',
  TODAYS_CHART = 'pp_todays_chart',
  SEARCH_CHART = 'pp_search_chart',
  CHART_REPORT = 'pp_chart_report',

  CAPTURE_GROUP = 'pp_capture_group',
  CAPTURE_TAG = 'pp_capture_tag',
  CAPTURE_BRAND = 'pp_capture_brand',
  CAPTURE_TYPE = 'pp_capture_type',
  CAPTURE_CONTACT = 'pp_capture_contacts',
  MANAGE_APPLE_IDS = 'pp_apple_ids',

  CAPTURE_SUPPLIER = 'pp_suppliers',
  CAPTURE_MEMBERS = 'pp_members',
  CAPTURE_USERS = 'pp_user',
  CAPTURE_CUSTOMERS = 'pp_customers',
  MANAGE_LOGINS = 'pp_manage_logins',
  EDIT_CONTACTS = 'pp_edit_contacts',
  MY_PROFILE = 'pp_profile',

}
