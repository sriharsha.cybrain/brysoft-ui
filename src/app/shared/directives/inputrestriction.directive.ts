import { Directive, ElementRef, HostListener, Injectable, Input } from '@angular/core';

@Directive({
  selector: '[appInputrestriction]'
})
export class InputrestrictionDirective {

  inputElement: ElementRef;

  @Input('appInputrestriction') appInputrestriction: string | undefined;
  arabicRegex = '[\u0600-\u06FF]';
  emailregex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  constructor(el: ElementRef) {
    this.inputElement = el;
  }

  @HostListener('keypress', ['$event']) onKeyPress(event: any) {

    if (this.appInputrestriction === 'integer') {
      this.integerOnly(event);
    }
    else if (this.appInputrestriction === 'noSpecialChars') {
      this.noSpecialChars(event);
    }
    else if (this.appInputrestriction === 'emailvalid') {
      this.emailidvalid(event);
    }
    else if (this.appInputrestriction === 'htmlinvalid') {
      this.htmlinvalid(event);
    }

  }

  integerOnly(event: KeyboardEvent) {

    const e = <KeyboardEvent>event;
    if (e.key === 'Tab' || e.key === 'TAB') {
      return;
    }
    if ([46, 8, 9, 27, 13, 110].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && e.ctrlKey === true) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && e.ctrlKey === true) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && e.ctrlKey === true) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && e.ctrlKey === true)) {
      // let it happen, don't do anything
      return;
    }
    if (['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'].indexOf(e.key) === -1) {
      e.preventDefault();
    }
  }

  noSpecialChars(event: KeyboardEvent) {
    const e = <KeyboardEvent>event;
    if (e.key === 'Tab' || e.key === 'TAB') {
      return;
    }
    let k;
    k = event.keyCode;  // k = event.charCode;  (Both can be used)
    if (k >= 48 && k <= 57) {

      e.preventDefault();
      return
    }
    if ((k > 64 && k < 91) || (k > 96 && k < 123) || k === 8 || k === 32 || k === 45 || (k >= 48 && k <= 57)) {
      return;
    }

    const ch = String.fromCharCode(e.keyCode);
    const regEx = new RegExp(this.arabicRegex);
    if (regEx.test(ch)) {
      e.preventDefault();
      return;
    }
    e.preventDefault();
  }
  emailidvalid(event: KeyboardEvent) {
    const e = <KeyboardEvent>event;
    let k;
    k = event.keyCode;  // k = event.charCode;  (Both can be used)
    if (k == 60 || k == 62 || k == 32) {
      e.preventDefault();
      return;
    }
    else {
      return;
    }
  }
  htmlinvalid(event: KeyboardEvent) {
    const e = <KeyboardEvent>event;
    let k;
    k = event.keyCode;  // k = event.charCode;  (Both can be used)
    if (k == 60 || k == 62) {
      e.preventDefault();
      return
    }
    else {
      return;
    }
  }

  @HostListener('paste', ['$event']) onPaste(event: ClipboardEvent) {

    let regex: RegExp | undefined | any;
    if (this.appInputrestriction === 'integer') {
      regex = /[0-9]/g;
    } else if (this.appInputrestriction === 'noSpecialChars') {
      regex = /[a-zA-Z -]/g;
    }
    else if (this.appInputrestriction === 'htmlinvalid') {
      regex = /<(.|\n)*?>/g;
    }
    else if (this.appInputrestriction === 'emailvalid') {
      regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/g;
    }

    const e = <ClipboardEvent>event as any;
    const pasteData = e.clipboardData.getData('text/plain');
    let m;
    let matches = 0;
    while ((m = regex.exec(pasteData)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      // The result can be accessed through the `m`-variable.
      m.forEach((match: any, groupIndex: any) => {
        matches++;
      });
    }
    if (this.appInputrestriction == 'integer' || this.appInputrestriction == 'noSpecialChars') {
      if (matches === pasteData.length) {
        return;
      } else {
        e.preventDefault();
      }
    }
    else if (this.appInputrestriction == 'htmlinvalid') {
      if (matches > 0) {
        e.preventDefault();
      } else {
        return;
      }
    }
    else {
      if (matches > 0) {
        return;

      } else {
        e.preventDefault();
      }
    }
  }
}
