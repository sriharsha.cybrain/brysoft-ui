import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import * as pipe from 'src/app/shared/pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { InputrestrictionDirective } from './directives/inputrestriction.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SortDirective } from './directives/sort.directive';

@NgModule({
  declarations: [
    pipe.OpenSearch,
    InputrestrictionDirective,
    SortDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    NgMultiSelectDropDownModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
    }),
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    NgbModule,
    pipe.OpenSearch,
    NgxPaginationModule,
    InputrestrictionDirective,
    SortDirective
  ],
})
export class SharedModule { }
