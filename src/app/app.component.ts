import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {


  data = ["R1", "R2"];



  constructor() {
    this.config();


  }

  public async config() {
    console.log(`%c
    
    %cWelcome to Brysoft , enjoy your day.`, 'color: #f33; font-weight: bold;', 'color: #777');


    console.group(
      '\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b Environment \b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\n'
    );
    console.log('Environment', environment.environment);
    console.log('Api', environment.Services.api_base_uri);
    console.groupEnd();
  }



}
